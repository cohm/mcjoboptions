evgenConfig.description = "Single electron with fixed pT = 10 GeV, flat eta (between -4.3 and 4.3), and flat phi"
evgenConfig.keywords = ["singleParticle", "electron"]

import ParticleGun as PG 
genSeq += PG.ParticleGun() 
evgenConfig.generators += ["ParticleGun"]
       
genSeq.ParticleGun.sampler.pid = 11
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=10000.0, eta=[-4.3, 4.3])
