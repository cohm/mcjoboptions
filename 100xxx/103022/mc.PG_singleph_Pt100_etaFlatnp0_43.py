evgenConfig.description = "Single photon with fixed pT = 100 GeV, flat eta (between -4.3 and 4.3), and flat phi"
evgenConfig.keywords = ["singleParticle", "photon"]

import ParticleGun as PG 
genSeq += PG.ParticleGun() 
evgenConfig.generators += ["ParticleGun"]
       
genSeq.ParticleGun.sampler.pid = 22
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=100000.0, eta=[-4.3, 4.3])
