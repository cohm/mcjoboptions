mH = 125
mfd2 = 5
mfd1 = 4
mZd = 400
nGamma = 2
avgtau = 50
decayMode = 'normal'
include("MadGraphControl_A14N23LO_FRVZdisplaced_wh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
