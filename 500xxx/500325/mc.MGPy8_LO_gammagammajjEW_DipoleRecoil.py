evgenConfig.description = "MadGraph+Pythia8 samples for gamma gamma + 2 jets EW production"
evgenConfig.contact = [ "josu.cantero.garcia@cern.ch" ]
evgenConfig.keywords = ["SM", 'gammagammajjEW']
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.nEventsPerJob = 10000

from MadGraphControl.MadGraphUtils import *

safefactor=2.5
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

#Defaults for run_card.dat
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'ickkw'         :'0',
           'pdlabel':"'lhapdf'",
           'lhaid'         : 303400,
           'drjj'          :'0.1',
           'drbb'          :'0.1',
           'drbj'          :'0.1',
           'draa'          :'0.2',
           'ptj'           :'10.',
           'ptgmin'        :'20.',
           'R0gamma'       :'0.1',
           'epsgamma'      :'0.1',
           'xn'            :'2.0',
           'isoEM'          :'False',
           'fixed_ren_scale' :'.false.',
           'fixed_fac_scale' :'.false.',
           'maxjetflavor'  : '5',
           'nevents'       : int(nevents)
           }  

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > a a j j  QCD=0 QED=4
output -f
"""

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras);

print_cards()
    
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3.0,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on',
                            'TimeShower:QEDshowerByGamma = off'
                            ]

