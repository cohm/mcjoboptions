include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'Shower ggf H->WW events with up to 2 partons and FxFx merging'
evgenConfig.contact = ['Dominik Duda <dominik.duda@cern.ch>']

genSeq.Pythia8.Commands += [ 'JetMatching:merge         = on',
                             'JetMatching:scheme        = 1',
                             'JetMatching:setMad        = off',
                             'JetMatching:qCut          = 30',
                             'JetMatching:coneRadius    = 1.0',
                             'JetMatching:etaJetMax     = 6.0',
                             'JetMatching:doFxFx        = on',
                             'JetMatching:qCutME        = 15.0',
                             'JetMatching:nJetMax       = 2' ]

genSeq.Pythia8.UserHooks = ['JetMatchingMadgraph']
genSeq.Pythia8.FxFxXS = True

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiElectronFilter("MultiElectronFilter")
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq += MultiLeptonFilter("MultiLeptonFilter")

MultiElectronFilter = filtSeq.MultiElectronFilter
MultiElectronFilter.Ptcut = 7000.
MultiElectronFilter.Etacut = 3.0
MultiElectronFilter.NElectrons = 1

MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut = 7000.
MultiMuonFilter.Etacut = 3.0
MultiMuonFilter.NMuons = 1

MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 16000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 1

filtSeq.Expression = "(MultiElectronFilter) and (MultiMuonFilter) and (MultiLeptonFilter)"

