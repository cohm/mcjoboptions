ATLAS MC JobOptions
====================

For any technical issues contact [atlas-phys-mcprod-jo@cern.ch](mailto:atlas-phys-mcprod-jo@cern.ch)

# Instructions for registering new jobOptions (MC contacts)

Before uploading new jO files to the repository, you need to make sure that you have tested the jO locally with `Gen_tf.py` and have included the `log.generate` produced by `Gen_tf.py` in the DSID directories you want to upload.  

**Important: if `log.generate` files are not provided, merge requests will in general not be accepted!**  

Once you have made sure that your jO run, the request has been discussed in an `ATLMCPROD` JIRA ticket and the relevant physics group is happy with the physics output, you can follow the next steps in order to
upload the jO to the repository.  

The repository is copied to `/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions` every 4 hours starting at 00.00 CERN time.

## Using the automatic commit script

The procedure describe below makes use of the automatic commit script `scripts/commit_new_dsid.sh`, which contains several options. To get a list of options and their explanation run  
```
./scripts/commit_new_dsid.sh -h
```

## Steps for booking DSIDs and uploading new jO

1.  For each jO you want to upload, prepare a dummy DSID directory outside the `500xxx-999xxx` range, (e.g. `100xxx/100000`) and add all the necessary files (jO, gridpacks, etc) in the directory. To upload the directories
to git it is **strongly recommended** that you use the automatic commit script `commit_new_dsid.sh` as described below.

2.  **Dry run of automatic commit script** . From the `mcjoboptions` directory run the script using the `--dry-run` flag:
```
./scripts/commit_new_dsid.sh -d=DSID1,DSID2,DSIDn-DSIDk -m="Commit message" --dry-run
```  
where `DSID1,DSID2` is a comma-separated list of the new DSID directories to be added to the commit. 
DSID ranges are also supported using a dash, e.g. `-d=421001-421003` will submit DSIDs 421001, 421002, 421003. It is also possible to combine individual DSIDs with a DSID range as indicated in the example above.  

The script will loop through the directories and 
*  check if the jO files in the DSID directories specified with the `-d` flag contain any naming errors. If they do, it will print out the ERRORs. The user should fix them and rerun the command above.
*  the script will identify the generator used in each jO and will search for the smallest continuous block of DSIDs that is not already used in any of the remote branches. It will then print out where each dummy DSID will be copied
(e.g. `Will move 999xxx/999995 to 800xxx/800045`)
*  locate the `log.generate` and check if there are no errors from the `logParser`. 
  If errors are found, you will not be allowed to commit to the repository and you will first have to fix the errors in your local jO file.  
  If no errors are found, the script will create a file called `log.generate.short` in every DSID directory you want to upload.  

**Before proceeding to the next step**:
*  **make sure that the list of DSIDs that the script suggested make sense to you**. If you are not sure contact the mailing list.
*  **check the list of files which will be added to the commit and make sure that it is as expected.** If not contact the mailing list.

3. **Assignment of DSIDs**: Assuming the dry run (step 2) was successful, run the commit script using the `-n` or `--nogit` flag:  
```
./scripts/commit_new_dsid.sh -d=DSID1,DSID2,DSIDn-DSIDk -m="Commit message" -n
```  
This will go through the same steps as step (2) above with the difference that now the **dummy DSIDs will actually be copied to their final location**. At the end the script should print out where each initial DSID
has been copied and it will suggest the command to use in order to commit the DSIDs to the repository, e.g.  
```
The following DSIDs have been assigned:
    999xxx/999995 -> 800xxx/800045
    999xxx/999996 -> 800xxx/800046
Run: ./scripts/commit_new_dsid.sh -d=800045-800046 -m="Commit message" to push them to git
```

4.  **Upload to git**: run the script as suggested by the previous command  
```
./scripts/commit_new_dsid.sh -d=800045-800046 -m="Commit message"
```  
This will upload all the necessary files to git in a new branch called `dsid_USER_800045`. If you want to specify a custom branch name you can use `-b=branchName` when running the script.  

5.  A CI pipeline will be launched with this commit, as described in the pipelines section below. **Make sure that the pipelines run and are successful!** If they don't run please contact the package maintainers. If they fail, look into the pipeline output to see why they fail and update your MR accordingly. If it's unclear why they fail, contact the package maintainers.  

6.  Create a merge request [here](https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/merge_requests) . The source branch should be the one created in step (2) above (e.g. `dsid_USER_DSID1`) and the target branch should be `atlas-physics/pmg/mcjoboptions/master`  

7.  Select the `new_jO` template in the title drop menu and fill in the description

## Skipping CI jobs

It is possible to skip certain CI jobs by manipulating the git commit messages, as described below in the Pipelines section.  
**This is in general discouraged and should only be performed by expert users. MR with skipped CI jobs will in general not be accepted!!!**  

To skip certain CI jobs run the commit script adding the `-m="[skip X],[skip Y]"` option, where `X,Y=all,modfiles,size,athena,logparser`, for example to commit the directory 800045 skipping the checks for modified files and the athena running do   
```
./scripts/commit_new_dsid.sh -d=800045 -m="Adding 800045 [skip modfiles] [skip athena]"
```

**Nota bene: Using the default gitlab rules to skip all pipelines, like `[skip ci]`, has the same effect as a failed pipeline, i.e. it leads to a merge request which cannot be merged according to the new gitlab policy for pipelines!**. 
The only way to skip all pipelines is to add `[skip all]` in the commit message, however this is reserved for expert users only and associated MRs will need approval from the PMG conveners.

# Accepting a MR with new jobOptions (jO coordinators)

Before accepting a MR make sure that all the items below have been completed successfully:  

* All the items in the todo list of the MR have been completed successfully
* The pipeline has run and the status is green
* CI jobs have not been skipped (if jobs have been skipped, confirmation from the PMG conveners is necessary before merging). 
* Look at the output of the `run_athena` pipeline and make sure that it has run for one of the DSIDs added to the commit. If it's not the case, confirmation from the PMG conveners is necessary before merging
* Check that no `log.generate.short` files are included in the commit to be merged to master. If such files are present, it indicates that something went wrong in the pipeline. Check the job logs and contact the package maintainers if needed.

# Instructions for developers

To push new changes to the repository follow the next steps:

1.  Check out the master branch and **before changing any file from the master** create a new branch with `git fetch --all && git checkout -b myBranch origin/master`  

2.  Add your changes with `git add ...`  

3.  Finally commit using an appropriate git message, e.g.  `git commit -m "appropriate message [skip modfiles]"` if you have modified pre-existing files residing in the DSID directories. Don't forget to push your changes using `git push origin myBranch`.  

4.  Create a merge request [here](https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/merge_requests) . The source branch should be the one created in step (1) above (e.g. `myBranch`) and the target branch should be `atlas-physics/pmg/mcjoboptions/master`. Explain what your MR does.  

5.  **Make sure to check the options "Delete source branch when merge request is accepted" and "Squash commits when merge request is accepted."**  

6.  **Make sure that when squashing commits an appropriate message is picked up with the correct CI tag** (by default the squashed commit message will coincide with the title of the MR, so if you want to skip certain CI jobs, you might need to add these in the squashed commit message by hand, if the title of the MR does not already contain the tag) 

# Pipelines

This repository uses CI pipelines that run the following jobs. A brief description of the pipelines is given below. Skipping pipelines is an expert operation restricted to the package maintainers. **No MR will be approved if pipelines have been skipped!**

1.  `check_jo_consistency`: 
*  **checks**: naming convention for jO files, length of jO file name, DSID ranges
*  **skippable**: NO  

2.    `check_unique_physicsShort`:
* **checks**: that there is no other jO file with the same physics short in the repository
* **skippable**: NO

3.    `check_modified_files`:
* **checks**: that no pre-existing files have been modified or deleted (excluding files listed in `scripts/modifiable.txt`) and that no files have been added in pre-existing DSID directories
* **skippable**: with `[skip modfiles]`

4.  `check_added_files:`
* **checks**: that the files added are jO files (named like `mc.*.py`), integration grids (named like `*.GRID.tar.gz`) or directories that contain control files for generators (these have to be named after the generator)
* **skippable**: NO

5.    `check_grid_readability`:
* **checks**: that the GRID files have been put on `/eos/atlas` or `/eos/user` in a directory that is readable by `atlcvmfs`, `mcgensvc` and if the files are on `cvmfs` that they are readable by all users
* **skippable**: NO

6.    `check_grid_size`:
* **checks**: that the GRID file size is less than 100 MB
* **skippable**: with `[skip size]`

7. `run_athena`:
* **checks**: that `Gen_tf.py` can run successfully with the uploaded jO files. `Gen_tf.py` will run **only if `log.generate.short` is provided with the commit**. 
* **skippable**: with `[skip athena]`

8.    `check_logParser`:
* **checks**: that a `log.generate` file exists for every new DSID directory added with a commit and that the `log.generate` contains no errors
* **skippable**: with `[skip logparser]`. The job is also not run when `[skip athena]` is used




