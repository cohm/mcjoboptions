#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig716 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, at least one lepton filter, ME NNPDF30 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.de','daniel.rauch@cern.ch','baptiste.ravina@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]


#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------                                                                                                                                                         
# Event filter                                                                                                                                                                                                            
#--------------------------------------------------------------                                                                                                                                                           
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
