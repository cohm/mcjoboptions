include("Sherpa_i/2.2.8_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa W+/W- -> mu nu + 0,1,2j@NLO + 3,4j@LO with 500 GeV < max(HT, ptV) < 1000 GeV with light-jet filter."
evgenConfig.keywords = ["SM", "W", "muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  %settings for MAX(HT,PTV) slicing
  SHERPA_LDADD=SherpaFastjetMAXHTPTV
  HTMIN:=500
  HTMAX:=1000
}(run)

(processes){
  Process 93 93 -> 13 -14 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

  Process 93 93 -> -13 14 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 13 -14 2.0 E_CMS
  Mass -13 14 2.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppln", "pplnj", "pplnjj" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]
genSeq.Sherpa_i.ExtraFiles += [ "libSherpaFastjetMAXHTPTV.so" ]

evgenConfig.nEventsPerJob = 200
