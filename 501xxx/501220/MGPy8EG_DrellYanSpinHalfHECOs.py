###############################################################                 

# Job options file for Evgen MadGraph Pythia8 HECO Generation               
# W.Y. Song, 2020-04-10                                                         
#==============================================================                 

#--------------------------------------------------------------                 
# MadGraph on the fly                                                           
#--------------------------------------------------------------                 
from MadGraphControl.MadGraphUtils import *

if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  beamEnergy = 6500.

massPoint = int(mass)
chargePoint = float(charge)
PdgId = 10000000+chargePoint*100
charge_int, charge_frac = divmod(chargePoint, 1) # integer part of chargePoint goes into charge_int (float), decimal part goes into charge_frac (float)
charge_frac *= 10
print 'Integer part of charge is', charge_int, ', fractional part is', charge_frac

# Define process
process_str="import model multichargedParticles\ngenerate p p > qb%ip%i qb%ip%i~\noutput -f" % (charge_int, charge_frac, charge_int, charge_frac)
process_dir = new_process(process_str)

# Create run card
run_card_extras = {}
run_card_extras['lhaid'] = '247000' # NNPDF23_lo_as_0130_qed                    
safefactor=1.1 #generate extra 10% events in case any fail showering            
run_card_extras['nevents'] = str(int(evgenConfig.nEventsPerJob*safefactor))
run_card_extras['pdlabel'] = 'lhapdf'
run_card_extras['lhe_version'] = '2.0'
run_card_extras['ptheavy'] = '%s' % ptcut
modify_run_card(process_dir=process_dir, settings=run_card_extras)

# Create param_card                                                                                 
param_card_extras = { "MULTICHARGEDPARTICLES": { 'MQb%ip%i' %(charge_int, charge_frac): massPoint}}
modify_param_card(process_dir=process_dir,params=param_card_extras)

print_cards()

# generating events in MG                                                     
generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=2, saveProcDir=True)

#--------------------------------------------------------------                 
# General MC15 configuration                                                    
#--------------------------------------------------------------                 
include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )
include( "Pythia8_i/Pythia8_MadGraph.py" )

#--------------------------------------------------------------                 
# Configuration for EvgenJobTransforms                                          
#--------------------------------------------------------------    
evgenConfig.description="Drell-Yan multi-charged particles generation for mass = %i GeV, charge = +/- %.1fe with MadGraph5_aMC@NLO+Pythia8, NNPDF23LO pdf and A14 tune in MC16" % (massPoint, chargePoint)
evgenConfig.keywords += ['exotic','drellYan','BSM','longLived']
evgenConfig.contact = ['wen.yi.song@cern.ch']
evgenConfig.specialConfig = "MASS=%i;CHARGE=%.1f;preInclude=SimulationJobOptions/preInclude.Qball.py;MDT_QballConfig=True" % (massPoint, chargePoint)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with HECO mass
#--------------------------------------------------------------
ALINE1="M %i                         %i.E+03       +0.0E+00 -0.0E+00 qb%ip%i             +" % (PdgId,massPoint,charge_int,charge_frac)
ALINE2="W %i                         0.E+00         +0.0E+00 -0.0E+00 qb%ip%i              +" % (PdgId,charge_int,charge_frac)

import os, sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
	os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2

#--------------------------------------------------------------
# Edit G4particle_whitelist.txt with HECOs
#--------------------------------------------------------------
ALINE3="%i   qb%ip%i  %i (Mev/c) lepton 0" %(PdgId,charge_int,charge_frac,1000*massPoint)
ALINE4="%i   qb%ip%ibar  %i (Mev/c) lepton 0" %((-1)*PdgId,charge_int,charge_frac,1000*massPoint)

pdgmod = os.path.isfile('G4particle_whitelist.txt')
if pdgmod is True:
	os.remove('G4particle_whitelist.txt')
os.system('get_files -data G4particle_whitelist.txt')
f=open('G4particle_whitelist.txt','a')
f.writelines(str(ALINE3))
f.writelines('\n')
f.writelines(str(ALINE4))
f.writelines('\n')
f.close()

del ALINE3
del ALINE4


