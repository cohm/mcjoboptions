#! /usr/bin/env python

import argparse
import optparse, sys, math, subprocess, os, re
from collections import OrderedDict

parser = optparse.OptionParser(usage=__doc__)
parser.add_option("-i", "--input", default="-", dest="INPUT_FILE", metavar="PATH",   help="path to input log.generate")
parser.add_option("-j", "--joFile", default=None, dest="JOFILE", metavar="PATH",   help="path to jO file")
parser.add_option("-u", "--nocpu", default=False, dest="SKIPCPU", action="store_true", help="Ignore CPU timing information.")
parser.add_option("-m", "--mcver", dest="MC_VER", default="mc", help="Specify MCXX campaign")
parser.add_option("-c", "--nocolour", action="store_true", dest="NO_COLOUR", default=False, help="Turn off colour for copying to file")
parser.add_option("-s", "--standalone", action="store_true", dest="STANDALONE", default=False, help="Run based on cvmfs location of files (stand-alone, no mcjoboptions locally)")

opts, fileargs = parser.parse_args()

MCXX='%s.'%opts.MC_VER
location = '/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions' if opts.STANDALONE else '.'
nEventsRequested=0

# define dictionaries with keys as variables to be searched for and values to store the results
JOsDict={
    'using release':[],
    'including file "'+MCXX:[]
}

generatorDict={
    '"nevents"':[],
    'No pdf base fragment':[]
}

testHepMCDict={
    'Events passed':[],
    'Efficiency':[]
}

countHepMCDict={
    'Events passing all checks and written':[]
}

evgenFilterSeqDict={
    'Weighted Filter Efficiency':[],
    'Filter Efficiency':[]
}

simTimeEstimateDict={
    'RUN INFORMATION':[]
}

metaDataDict={
    'physicsComment =':[],
    'generatorName =':[],
    'generatorTune':[],
    'specialConfig =':[],
    'contactPhysicist =':[],
    'genFilterNames = ':[],
    'cross-section (nb)':[],
    'generator =':[],
    'weights =':[],
    'PDF =':[],
    'GenFiltEff =':[],
    'sumOfNegWeights =':[],
    'sumOfPosWeights =':[]
}

generateTfDict={
    'ecmEnergy':[],
    'nEventsPerJob':[],
    'Requested output events':[],
    'transform':[],
    'inputFilesPerJob':[],
    'inputGeneratorFile':[]
}

perfMonDict={
    'snapshot_post_fin':[],
    'jobcfg_walltime':[],
    'last -evt vmem':[]
}

testDict = {
    'TestHepMC':testHepMCDict,
    'EvgenFilterSeq':evgenFilterSeqDict,
    'CountHepMC':countHepMCDict,
    'SimTimeEstimate':simTimeEstimateDict
}

# Function to get jO includes
def getJOsList():
    liststr=''
    if len(JOsDict["including file \""+MCXX]):
        if len(liststr): liststr+="|"
        liststr+="|".join(JOsDict["including file \""+MCXX]).replace("nonStandard/","")
    liststr=liststr.replace('/','').replace('"','').replace('including file','').replace(' ','')
    tmplist=liststr.split('|')
    return tmplist

# Function to check blacklist
def checkBlackList(relFlavour,cache,generatorName,location) :
    isError = None
    with open(location+'/common/BlackList_caches.txt') as bfile:
        for line in bfile.readlines():
            if not line.strip():
                continue
            # Blacklisted release flavours
            badRelFlav=line.split(',')[0].strip()
            # Blacklisted caches
            badCache=line.split(',')[1].strip()
            # Blacklisted generators
            badGens=line.split(',')[2].strip()
            
            #Match Generator and release type e.g. AtlasProduction, MCProd
            if relFlavour==badRelFlav and cache==badCache and re.search(badGens,generatorName) is not None:
                isError=f"{relFlavour},{cache} is blacklisted for {generatorName}"
                return isError
    return isError

# Function to parse log.generate lines using a given identifier - result is stored in dictionary
def checkLine(line, lineIdentifier, dict, splitby):
    if lineIdentifier in line:
        for param in dict:
            if param=="including file \""+MCXX:
                if "including file" in line and MCXX in line:
                    if len(line.split(splitby))==0:
                        raise RuntimeError("Found bad entry %s"%line)
                    else:
                        thing="".join(line.split(lineIdentifier)[1].split(splitby)[1:]).split("/")[-1].strip()
                        dict[param].append(thing)
                    break
            elif param=="Requested output events":
                if "Requested output events" in line:
                   if len(line.split(splitby))==0:
                       raise RuntimeError("Found bad entry %s"%line)
                   else:
                       thing="".join(line.split(lineIdentifier)[1].split(" ")[-1]).strip()
                       dict[param].append(thing)
                   break
            elif param=="inputGeneratorFile":
                if "inputGeneratorFile" in line:
                    thing="".join(line.split(" used ")[-1]).strip()
                    dict[param].append(thing)
            else:
                if param in line:
                    if len(line.split(splitby))==0:
                        raise RuntimeError("Found bad entry %s"%line)
                    else:
                        thing="".join(line.split(lineIdentifier)[1].split(splitby)[1:]).strip()
                        dict[param].append(thing)
                    break

# For printing
class bcolors:
    if not opts.NO_COLOUR:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
    else:
        HEADER = ''
        OKBLUE = ''
        OKGREEN = ''
        WARNING = ''
        FAIL = ''
        ENDC = ''

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

# For counting errors/warnings
class LogCounts:
    Errors = 0
    Warnings = 0

# Functions that print coloured output
def loginfo(out1,out2):
    print(f"{out1:s}{bcolors.OKBLUE} {out2:s}{bcolors.ENDC}")
def loggood(out1,out2):
    print(f"{out1:s}{bcolors.OKGREEN} {out2:s}{bcolors.ENDC}")
def logerr(out1,out2):
    print(f"{out1:s}{bcolors.FAIL} {out2:s}{bcolors.ENDC}")
    LogCounts.Errors += 1
def logwarn(out1,out2):
    print(f"{out1:s}{bcolors.WARNING} {out2:s}{bcolors.ENDC}")
    LogCounts.Warnings += 1

def pad(seq, target_length, padding=None):
    length = len(seq)
    if length > target_length:
        return seq
    seq.extend([padding] * (target_length - length))
    return seq

# Functions for generator-specific tests
# Sherpa checks
def sherpaChecks(logFile):
    file=open(logFile,"r")
    lines=file.readlines()    
    # check each line
    inside = 0
    numexceeds =0
    retriedBlock=False
    for line in lines:
        if "exceeds maximum by" in line:
            numexceeds +=1
            loginfo("- "+line.strip(),"")
        if "Retried events" in line:
            retriedBlock = True
            continue
        if retriedBlock:
            if "}" in line:
                retriedBlock=False
                continue
            if len(line.split('"')) == 1 or len(line.split('->'))== 1:
                continue
            name = line.split('"')[1]
            percent = line.split('->')[1].split("%")[0].strip()
            if float(percent) > 5.:
                logwarn("- retried events "+name+" = ",percent+" % <-- WARNING: more than 5% of the events retried")
            else:
                loginfo("- retried events "+name+" = ",percent+" %")
    if numexceeds*33>int(nEventsRequested):
        logwarn("","WARNING: be aware of: "+str(numexceeds*100./nEventsRequested)+"% of the event weights exceed the maximum by a factor of ten")

# Pythia 8 checks
def pythia8Checks(logFile,generatorName):
    file=open(logFile,"r")
    lines=file.readlines()
    usesShowerWeights = False
    usesMatchingOrMerging = False
    usesCorrectPowheg = False
    errors = False
    for line in lines:
        if "Pythia8_ShowerWeights.py" in line:
            usesShowerWeights = True
        if "Pythia8_aMcAtNlo.py" in line or "Pythia8_CKKWL_kTMerge.py" in line or "Pythia8_FxFx.py" in line:
            usesMatchingOrMerging = True
        if "Pythia8_Powheg_Main31.py" in line:
            usesCorrectPowheg = True
    if usesShowerWeights and usesMatchingOrMerging:
        logerr("ERROR:","Pythia 8 shower weights buggy when using a matched/merged calculation. Please remove the Pythia8_ShowerWeights.py include.")
        errors = True
    if "Powheg" in generatorName and not usesCorrectPowheg:
        logerr("ERROR:",generatorName+" used with incorrect include file. Please use Pythia8_Powheg_Main31.py")
        errors = True
    if not errors:
        loggood("INFO: Pythia 8 checks:","Passed")		

# Herwig 7 checks
def herwig7Checks(logFile,generatorName):
    errors = False
    allowed_tunes=['H7.1-Default', 'H7.1-SoftTune', 'H7.1-BaryonicReconnection']
    if "7.1" in generatorName:
        if metaDataDict['generatorTune'][0] not in allowed_tunes:
            logerr("ERROR:", f"Metadata tune set to {metaDataDict['generatorTune'][0]}, which is not in the list of allowed tunes: {allowed_tunes}")
            errors = True
        file=open(logFile,"r")
        lines=file.readlines()
        for line in lines:
            if "Herwig7_EvtGen.py" in line:
                logerr("ERROR:","Herwig 7.1 used with wrong include: Herwig7_EvtGen.py. Please use Herwig71_EvtGen.py instead.")
                errors = True
                break
    if not errors:
        loggood("INFO: Herwig 7 checks:","Passed")

# Madgraph checks
def madgraphChecks(logFile):
    errors=False
    # Check that the events that MG generates are 10% more than nEventsPerJob
    if not generateTfDict['inputGeneratorFile']: # This check only makes sense if no external LHE inputs are used
        try:
            neventsMG=int(float(generatorDict['"nevents"'][0]))
        except:
            raise RuntimeError("nevents was not set in the MG5aMC jO")
        if neventsMG < int(1.1*nEventsRequested):
            logerr("ERROR:",f"Increase nevents to be generated in MG from {neventsMG} to {int(1.1*nEventsRequested)}")
            errors=True
    # Check if PDF base fragments were included
    if generatorDict['No pdf base fragment']:
        logwarn("WARNING:","No PDF base fragment was included, which is the recommended way to steer pdf and systematics (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)")
    file=open(logFile,"r")
    lines=file.readlines()
    for line in lines:
        if "event_norm" in line and 'sum' in line and not 'average' in line:
            logwarn("WARNING:","The use of event_norm=sum will almost always result in the sample having the wrong total cross section -- please double check that event_norm=average is set in the param_card.dat.")
        if "We need to recalculate the branching fractions" in line:
            br_particles=[p.strip() for p in line.split('for')[-1].split(',')]
            bad_br_particles=[p for p in br_particles if p in ['t','t~','w+','w-','z','h']]
            if len(bad_br_particles)>0:
                logwarn("WARNING:","MadWidth is used to calculate the branching ratios of {}. This is only LO accurate. For more accurate BRs, please set them explictly in the param_card.dat.".format(",".join(bad_br_particles)))
    if not errors:
        loggood("INFO: MadGraph checks:","Passed")


# Function to extract evgenConfig variables from jO file
def getNEventsFromJO(jOpath):
    locals = {"evgenConfig": argparse.Namespace()}
    try:
        with open(jOpath) as jOFile:
            for line in jOFile.readlines():
                if "os.system" in line: continue # for security
                try:
                    exec(line, {}, locals)
                except:
                    pass
        # Also check that there are no evgenConfig.minevents lines in the jO file
        if hasattr(locals["evgenConfig"], "minevents"):
            logerr("ERROR:", f"{jOpath} is using deprecated parameter evgenConfig.minevents. Please switch to evgenConfig.nEventsPerJob")
        if hasattr(locals["evgenConfig"], "nEventsPerJob"):
            return getattr(locals["evgenConfig"], "nEventsPerJob")
        else:
            logwarn("WARNING:", f"evgenConfig.nEventsPerJob is not defined in the jO. Will set to default=10000")
            return 10000
    except:
        raise RuntimeError(f"Problem while reading {jOpath}. If the file doesn't exist consider running without -j /path/to/joFile")


# Main function
def main():
    """logParser.py script for parsing log.generate files to check MC production settings and output
     - Written by Josh McFayden <mcfayden@cern.ch> Nov 2016 """
    
    global nEventsRequested

    if opts.INPUT_FILE=="-":
        parser.print_help()
        return 
    
    # open and read log file
    file=open(opts.INPUT_FILE,"r")
    lines=file.readlines()

    # check each line
    for line in lines:
        checkLine(line,'Py:Athena',JOsDict,'INFO')
        checkLine(line,'MetaData',metaDataDict,'=')
        checkLine(line,'Py:Gen_tf',generateTfDict,'=')
        checkLine(line,'Py:PerfMonSvc',perfMonDict,':')
        checkLine(line,'PMonSD',perfMonDict,'---')
        checkLine(line,'TestHepMC',testHepMCDict,'=')
        checkLine(line,'Py:EvgenFilterSeq',evgenFilterSeqDict,'=')
        checkLine(line,'CountHepMC',countHepMCDict,'=')
        checkLine(line,'SimTimeEstimate',simTimeEstimateDict,'|')
        checkLine(line,'Py:MadGraphUtils',generatorDict,'=')
        checkLine(line,'Py:MadGraphSysUtils',generatorDict,'WARNING !!!')
        
    ### jO checks
    JOsErrors=[]
    print("")
    print("---------------------")
    print("jobOptions and release:")
    print("---------------------")

    # Checking jobOptions
    JOsList=getJOsList()
    if not len(JOsList):
        JOsErrors.append("including file \""+MCXX)
    else:
        if not len(JOsDict["including file \""+MCXX]):
            JOsErrors.append("including file \""+MCXX)

    # Extract generator name
    generatorName=metaDataDict['generatorName ='][0]

    # Checking release
    release="not found"
    if not len(JOsDict['using release']):
        JOsErrors.append(JOsDict['using release'])
    else:
        name='using release'
        tmp=JOsDict[name][0].replace('using release','').strip().split()[0]
        val=tmp.replace('[','').replace(']','')
        flavour=val.split('-')[0]
        release=val.split('-')[1]
        blacklisted=checkBlackList(flavour,release,generatorName,location=location)
        # check that release is AthGeneration
        if flavour != "AthGeneration":
            logerr( '- '+name+' = ',"".join(val)+f" <-- ERROR: AthGeneration should be used instead of {flavour}")
        else:
            # check blacklist
            if blacklisted:
                logerr( '- '+name+' = ',"".join(val)+" <-- ERROR: %s"%blacklisted)
            else:
                loggood( '- '+name+' = ',"".join(val))
     
    
    if len(JOsErrors):
        print("---------------------")
        print("MISSING JOs:")
        for i in JOsErrors:
            if i == "including file \""+MCXX:
                logerr("",f"ERROR: jO not found! (log.generate should contain lines like: including file \"{MCXX}*.py\")")
            else:
                logwarn("","WARNING: %s is missing!"%i)
    
    
    ### Generate transform checks
    generateErrors=[]
    print("")
    print("---------------------")
    print("Generate transform params:")
    print("---------------------")

    # add default energy - for backwards compatibility otherwise logParser would crash - to fix
    generateTfDict["ecmEnergy"].append("13000")

    for key in list(generateTfDict.keys()):
        optionalTests=['inputGeneratorFile']
        val=generateTfDict[key]
        if not len(val):
            if key not in optionalTests:
                generateErrors.append(name)
        else:
            if key == 'nEventsPerJob':
                # Allow to overwrite nEventsPerJob from jO file if specified (or present)
                if opts.JOFILE:
                    nEventsPerJob=str(getNEventsFromJO(opts.JOFILE))
                else:
                    # No jO file specified, still try to overwrite from JO in same dir
                    jOFile=os.path.join(location,os.path.dirname(opts.INPUT_FILE),JOsList[0])
                    if os.path.exists(jOFile):
                        nEventsPerJob=str(getNEventsFromJO(jOFile))
                    else:
                        nEventsPerJob=str(val[0]).split('#')[0].strip()
                val=nEventsPerJob
                generateTfDict['nEventsPerJob'] = val
            elif key == 'transform':
                val=val[0]
                releaseNumber=int(release.split(".")[0])*10000+int(release.split(".")[1])*100+int(release.split(".")[2])
                if (val != 'Gen_tf' and val != 'Gen_tf_txt') and releaseNumber > 210610:
                    logerr(f"- {key} = ",f"{val} <- ERROR: tranform = {val} and release is {release}. Please use Gen_tf or Gen_tf_txt!")
                    continue
            elif key == 'inputFilesPerJob':
                nfiles=val[0].split("#")[0]
                val=nfiles
                if int(val) > 100:
                    # Hard limit from ADC is 1000 files
                    logerr(f"- {key} = ",f"{val} <- ERROR: Need to use less than 100.")
                    continue
            elif key == "ecmEnergy" or key == "Requested output events" or key == "inputGeneratorFile":
                val=val[0]
            loginfo(f"- {key} = ",f"{val}")
    
    if len(generateErrors):
        print("---------------------")
        print("MISSING Generate params:")
        for i in generateErrors:
            logerr("","ERROR: %s is missing!"%i)
            
    # Number of requested output events
    nEventsRequested=int(generateTfDict["Requested output events"][0])
    
    ### Metadata checks
    metaDataErrors=[]
    print("")
    print("---------------------")
    print("Metadata:")
    print("---------------------")
    for key in metaDataDict:
        name=key.replace("=","").strip()
        val=metaDataDict[key]
        if not len(val):
            metaDataErrors.append(name)
        else:
            if name=="contactPhysicist":
                if '@' in "".join(val):
                    loggood( '- '+name+' = ',"".join(val))
                else:
                    logerr( '- '+name+' = ',"".join(val)+"  <-- ERROR: No email found")
                continue
            elif name=="cross-section (nb)":
                if float(val[0]) < 0:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: Cross-section is negative")
                    continue
            loginfo( '- '+name+' = ',"".join(val))
    
    if len(metaDataDict["sumOfPosWeights ="]) and len(metaDataDict["sumOfNegWeights ="]):
        ratio = float(metaDataDict["sumOfNegWeights ="][0])*1.0/(float(metaDataDict["sumOfPosWeights ="][0]) + float(metaDataDict["sumOfNegWeights ="][0]))
        if ratio>0.15:
            logwarn( '- sumOfNegWeights/(sumOfPosWeights+sumOfNegWeights) = ',str(ratio)+"  <-- WARNING: more than 15% of the weights are negative")


    if len(metaDataErrors):
        print("---------------------")
        print("MISSING Metadata:")
        for i in metaDataErrors:
            if i=="weights" or i=="genFilterNames" or i=="generator" or i=="PDF" or i=="sumOfNegWeights" or i=="sumOfPosWeights":
                loginfo("INFO:","%s is missing"%i)
            else:
                logerr("","ERROR: %s is missing!"%i)
            
    ### Generator specific tests
    print("")
    print("-------------------------")
    print(f"Generator specific tests: {generatorName}")
    print("-------------------------")
    if "Sherpa" in generatorName:
        sherpaChecks(opts.INPUT_FILE)
    if "Pythia8" in generatorName:
        pythia8Checks(opts.INPUT_FILE,generatorName)
    if "Herwig7" in generatorName:
        herwig7Checks(opts.INPUT_FILE,generatorName)
    if "MadGraph" in generatorName or "aMcAtNlo" in generatorName:
        madgraphChecks(opts.INPUT_FILE)

    ###  Event tests
    testErrors=[]
    filt_eff=1.0
    CountHepMC=0
    print("")
    print("---------------------")
    print("Event tests:")
    print("---------------------")
    for dictkey in testDict:
        for key in testDict[dictkey]:
            name=key
            val=testDict[dictkey][key]
            if not len(val):
                testErrors.append("%s %s"%(dictkey,name))
            else:
                #Check final Nevents processed
                if dictkey=="CountHepMC":
                    nEventsAllowedInProduction = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000]
                    CountHepMC=int(val[0])
                    tmp=CountHepMC if CountHepMC == int(nEventsPerJob) else int(nEventsPerJob)
                    if not tmp in nEventsAllowedInProduction:
                        logerr( '- '+dictkey+" "+name+' = ', f"{tmp}  <-- ERROR: Not an acceptable number of events for production (1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000)")
                    elif CountHepMC != nEventsRequested:
                        logerr( '- '+dictkey+" "+name+' = ', f"{val[0]}  <-- ERROR: This is not equal to Requested output events={nEventsRequested}")
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
                if dictkey=="TestHepMC" and name=="Efficiency":
                    if float(val[0].replace('%',''))<100. and float(val[0].replace('%',''))>=98.:
                        logwarn( '- '+dictkey+" "+name+' = ',"".join(val))
                    elif float(val[0].replace('%',''))<100.:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val))
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
                loginfo( '- '+dictkey+" "+name+' = ',"".join(val))
    
    if len(testErrors):
        print("---------------------")
        print("Failed tests:")
        for i in testErrors:
            if i =="SimTimeEstimate RUN INFORMATION":
                logwarn("","WARNING: %s is missing!"%i)
            else:
                if "TestHepMC" in i and "Sherpa" in metaDataDict['generatorName ='][0]:
                    logwarn("","WARNING: %s is missing, but expected as it's Sherpa!"%i)
                else:
                    logerr("","ERROR: %s is missing!"%i)


    ### Performance tests
    cpuPerJob=0.0
    perfMonErrors=[]
    print("")
    print("---------------------")
    print("Performance metrics:")
    print("---------------------")
    for key in perfMonDict:
        name=key
        val=perfMonDict[key]
        if not len(val):
            perfMonErrors.append(name)
        else:
            if key == 'snapshot_post_fin' and not opts.SKIPCPU:
                name = 'CPU'
                tmp = 0.
                tmp=float(val[0].split()[3])
                if len(perfMonDict['jobcfg_walltime']):
                    tmp+=float(perfMonDict['jobcfg_walltime'][0].split()[1].split('=')[1])
                cpuPerJob=tmp/(1000.*60.*60.)
                # Calculate timing and extrapolate if test run was run with less events
                if CountHepMC != int(nEventsPerJob):
                    print(f"- actual CPU ({CountHepMC} events) = {cpuPerJob:.2f} hrs")
                    cpuPerJob=float(nEventsPerJob)*cpuPerJob/float(CountHepMC)
                    loginfo(f"- CPU extrapolated to {nEventsPerJob} events =", f"{cpuPerJob:.1f} hrs")
                if cpuPerJob > 18.:
                    logerr( f"- {name} = ",f"{cpuPerJob:.2f} hrs  <-- ERROR: Too high CPU time - should be between 6-12h. Adjust nEventsPerJob!")
                elif cpuPerJob >= 6. and cpuPerJob <= 12.:
                    loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                elif cpuPerJob < 1.:
                    if ((CountHepMC == int(nEventsPerJob) and CountHepMC < 10000) or
                        (CountHepMC != int(nEventsPerJob) and int(nEventsPerJob) < 10000)):
                        logerr( f"- {name} = ",f"{cpuPerJob:.2f} hrs <-- ERROR: Too low CPU time - should be between 6-12h. Adjust nEventsPerJob!")
                    else:
                        loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                else:
                    if ((CountHepMC == int(nEventsPerJob) and CountHepMC < 10000) or
                        (CountHepMC != int(nEventsPerJob) and int(nEventsPerJob) < 10000)):
                        logwarn( f"- {name} = ",f"{cpuPerJob:.2f} hrs  <-- WARNING: CPU time not optimal - should be between 6-12h. Adjust nEventsPerJob!")
                    else:
                        loggood( f"- {name} = ",f"{cpuPerJob:.2f} hrs")
                # Also print timing information for CI - CI runs max(1,0.01*nEventsPerJob)
                CICPU=max(1,0.01*float(nEventsPerJob))*cpuPerJob/float(nEventsPerJob)
                print(f"- estimated CPU for CI job = {CICPU:.2f} hrs")
                
            if key == 'last -evt vmem':
                name = 'Virtual memory'
                tmp=float(val[0].split()[0])
                if tmp > 4000 and tmp < 8000:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: High memory usage - alert MC production team")
                elif tmp > 8000:
                    logerr( '- '+name+' = ',"".join(val)+"  <-- ERROR: Too high memory usage")
                else:
                    loggood( '- '+name+' = ',"".join(val))
    
    
    if len(perfMonErrors):
        print("---------------------")
        print("MISSING Performance metric:")
        for i in perfMonErrors:
            logerr("ERROR:", f"{i} is missing!")
            
    
    # Print total number of Errors/Warnings
    print("")
    print("---------------------")
    print(" Summary:")
    print("---------------------")
    if (LogCounts.Errors == 0):
        if (LogCounts.Warnings == 0):
            loggood("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> OK for production")
        else:
	        logwarn("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Some warnings encountered, check that these are ok before submitting production!")
    else:
        logerr("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Errors encountered! Not ready for production!")  
    print("")
        
    return 
    
    
if __name__ == "__main__":
    main()
    
    
