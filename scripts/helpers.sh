# Helper functions to print coloured messages in the terminal
# Example usage:
# Simple message:
# printError "error message"
# Formatted message (use -f flag)
# printError -f "A string: %30s %5d" $string $number1

printError() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;31m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;31m%s\033[0m\n" "$1"
  fi
}

printWarning() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;33m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;33m%s\033[0m\n" "$1"
  fi
}

printGood() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;32m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;32m%s\033[0m\n" "$1"
  fi
}

printInfo() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;34m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;34m%s\033[0m\n" "$1"
  fi
}

