include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ReducedShowerWeights.py")

evgenConfig.description = "Pythia8 gamma+jet events with prompt (direct) photons in 35 < pT_ylead < 50"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.contact = ["frank.siegert@cern.ch", "ana.cueto@cern.ch"]

genSeq.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 17.5"]

include("GeneratorFilters/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = [ 35000. ]
filtSeq.DirectPhotonFilter.Ptmax = [ 50000. ]
filtSeq.DirectPhotonFilter.OrderPhotons = True
