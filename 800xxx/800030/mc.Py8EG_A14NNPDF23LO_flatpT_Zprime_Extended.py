evgenConfig.description = "Pythia8 Z' -> bb, cc, ss, uu, dd, ee, tautau, with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "Z' -> bb/cc/ss/uu/dd/ee/tautau"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]

include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.UserHooks += ["ZprimeFlatpT"]

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z bosons
                            "PhaseSpace:mHatMin = 25.0",           # minimum inv.mass cut
                            "32:onMode = off",                     # switch off all Z' decays
                            "32:m0 = 4000.0"]                      # set Z' mass

genSeq.Pythia8.Commands += ['32:oneChannel = on 0.30 100 -5 5'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.30 100 -4 4'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.10 100 -3 3'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.10 100 -2 2'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.10 100 -1 1'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.05 100 -15 15'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.05 100 -11 11'] # onMode bRatio meMode products

#Only Z' - no gamma/Z
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3",
                            "ZprimeFlatpT:MaxSHat=13000.",
                            "ZprimeFlatpT:DoDecayWeightBelow=0."]

testSeq.TestHepMC.MaxVtxDisp = 1500.0

include("Pythia8_i/Pythia8_ShowerWeights.py")
